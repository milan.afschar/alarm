import psutil
import logging
import smtplib
from email.mime.text import MIMEText

# Soft- und Hardlimits für CPU-Auslastung und Speichernutzung
soft_cpu_limit = 80
hard_cpu_limit = 90
soft_memory_limit = 80
hard_memory_limit = 90

# E-Mail-Einstellungen
smtp_server = 'smtp.gmail.com'
smtp_port = 587
smtp_username = 'your-email@gmail.com'
smtp_password = 'your-email-password'
sender_email = 'your-email@gmail.com'
recipient_email = 'recipient-email@gmail.com'

# Log-Einstellungen
logging.basicConfig(filename='system_log.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Überprüfung der CPU-Auslastung
cpu_percent = psutil.cpu_percent()
if cpu_percent >= hard_cpu_limit:
    # E-Mail senden
    message = MIMEText(f"CPU-Auslastung beträgt {cpu_percent}% und hat den Hardlimitwert überschritten.")
    message['From'] = sender_email
    message['To'] = recipient_email
    message['Subject'] = "CPU-Alarm"
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(smtp_username, smtp_password)
        server.sendmail(sender_email, recipient_email, message.as_string())
    # Logeintrag erstellen
    logging.error(f"CPU-Auslastung beträgt {cpu_percent}% und hat den Hardlimitwert überschritten.")
elif cpu_percent >= soft_cpu_limit:
    # Logeintrag erstellen
    logging.warning(f"CPU-Auslastung beträgt {cpu_percent}% und hat den Softlimitwert überschritten.")

# Überprüfung der Speichernutzung
memory_usage = psutil.virtual_memory().percent
if memory_usage >= hard_memory_limit:
    # E-Mail senden
    message = MIMEText(f"Speichernutzung beträgt {memory_usage}% und hat den Hardlimitwert überschritten.")
    message['From'] = sender_email
    message['To'] = recipient_email
    message['Subject'] = "Speicher-Alarm"
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(smtp_username, smtp_password)
        server.sendmail(sender_email, recipient_email, message.as_string())
    # Logeintrag erstellen
    logging.error(f"Speichernutzung beträgt {memory_usage}% und hat den Hardlimitwert überschritten.")
elif memory_usage >= soft_memory_limit:
    # Logein
