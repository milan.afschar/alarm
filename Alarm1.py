import psutil

def get_disk_usage():
    return psutil.disk_usage('/').percent

def get_process_count():
    return len(psutil.pids())

# weitere Funktionen zur Erfassung von Messdaten können hier hinzugefügt werden

def main():
    # Schwellenwerte und Meldungstexte
    disk_soft_limit = 80
    disk_hard_limit = 90
    disk_warning_message = "Der Festplattenspeicher ist fast voll ({}%)."
    disk_alert_message = "Der Festplattenspeicher ist voll ({}%)!"
    process_soft_limit = 100
    process_hard_limit = 150
    process_warning_message = "Die maximale Anzahl der Prozesse ({}) ist fast erreicht."
    process_alert_message = "Die maximale Anzahl der Prozesse ({}) ist erreicht!"

    # Monitoring
    while True:
        # Messdaten erfassen
        disk_usage = get_disk_usage()
        process_count = get_process_count()

        # Alarm-System aufrufen
        if disk_usage >= disk_hard_limit:
            send_email("Festplattenspeicher voll", disk_alert_message.format(disk_usage))
            log_message(disk_alert_message.format(disk_usage))
        elif disk_usage >= disk_soft_limit:
            log_message(disk_warning_message.format(disk_usage))
        if process_count >= process_hard_limit:
            send_email("Maximale Anzahl der Prozesse erreicht", process_alert_message.format(process_count))
            log_message(process_alert_message.format(process_count))
        elif process_count >= process_soft_limit:
            log_message(process_warning_message.format(process_count))
        
        # Intervall bis zur nächsten Messung
        time.sleep(60)

if __name__ == "__main__":
    main()


import smtplib
from email.mime.text import MIMEText

def check_threshold(value, soft_limit, hard_limit, warning_message, alert_message):
    if value >= hard_limit:
        send_email("Alarm", alert_message)
        log_message(alert_message)
    elif value >= soft_limit:
        log_message(warning_message)

def send_email(subject, message):
    sender = 'afschar37@gmail.com'
    recipient = 'mhassanzadah956@gmail.com'
    smtp_server = 'smtp.gamil.com'
    smtp_port = 587 (TLS) or 465 (SSL)
    smtp_username = 'afschar37@gmail.com'
    smtp_password = ''

    msg = MIMEText(message)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = recipient

    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.ehlo()
        server.starttls()
        server.login(smtp_username, smtp_password)
        server.sendmail(sender, recipient, msg.as_string())

def log_message(message):
    with open('logfile.txt', 'a') as f:
        f.write("{} - {} - {}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), socket.gethostname(), message))
