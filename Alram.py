import psutil # Installieren Sie psutil, um Systemressourcen abzurufen.
import logging
import smtplib

# Definieren Sie die Grenzwerte für den Soft- und Hardlimit
SOFT_LIMIT = 80.0 # % des belegten Plattenplatzes
HARD_LIMIT = 200 # maximale Anzahl der Prozesse

# Ermitteln Sie den aktuellen Wert
disk_usage = psutil.disk_usage('/') # In Prozent
num_processes = len(psutil.pids())

# Überprüfen Sie, ob der Wert den Softlimit überschreitet
if disk_usage.percent > SOFT_LIMIT:
    logging.warning('Plattenspeicher zu %d%% belegt', disk_usage.percent)
    logging.info('Eintrag in der Logdatei erstellt.')

    # Überprüfen Sie, ob der Wert den Hardlimit überschreitet
    if num_processes > HARD_LIMIT:
        logging.error('Maximale Anzahl der Prozesse überschritten: %d', num_processes)
        logging.info('Eintrag in der Logdatei erstellt.')
        
        # Senden Sie eine E-Mail
        sender_email = 'afschar37@gmail.com'
        receiver_email = 'asnzada@outlook.com'
        message = 'Warnung: Maximale Anzahl der Prozesse überschritten!'
        smtp_server = 'smtp.example.com'
        smtp_port = 587
        smtp_username = 'username'
        smtp_password = 'password'

        with smtplib.SMTP(smtp_server, smtp_port) as server:
            server.starttls()
            server.login(smtp_username, smtp_password)
            server.sendmail(sender_email, receiver_email, message)

 